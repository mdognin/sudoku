from collections import defaultdict
from dataclasses import dataclass
from nis import cat
import time
from typing import List

@dataclass(frozen=True)
class Index:
    row: int
    col: int

class Constraint(object):
    # A constraint is a list of indices. The constraint is that over all these indices:
    # - the value is either 0 (not set) or in [1..9]
    # - we can have at most one appearance of each of the values in [1..9]
    def __init__(self, indexes: List[Index]):
        self.indexes = indexes
        assert len(indexes) == 9


class SudokuMatrix(object):
    all_constraints = []
    constraints_by_index = defaultdict(list)
    for row in range(0, 9):
        indexes = []
        for col in range(0, 9):
            indexes.append(Index(row, col))
        constraint = Constraint(indexes)
        all_constraints.append(constraint)
        for index in indexes:
            constraints_by_index[index].append(constraint)
    for col in range(0, 9):
        indexes = []
        for row in range(0, 9):
            indexes.append(Index(row, col))
        constraint = Constraint(indexes)
        all_constraints.append(constraint)
        for index in indexes:
            constraints_by_index[index].append(constraint)
    for square_row in range(0, 3):
        for square_col in range(0, 3):
            indexes = []
            for row in range(square_row * 3, square_row * 3 + 3):
                for col in range(square_col * 3, square_col * 3 + 3):
                    indexes.append(Index(row, col))
            constraint = Constraint(indexes)
            all_constraints.append(constraint)
            for index in indexes:
                constraints_by_index[index].append(constraint)
    assert len(constraints_by_index) == 81
    assert len(all_constraints) == 27
    
    
    def __init__(self, representation: List[List[str]]):
        self.cells = []
        for subline in representation:
            assert len(subline) == 1
            string_representation = subline[0]
            subparts = string_representation.split(" ")
            assert len(subparts) == 3
            for subpart in subparts:
                assert len(subpart) == 3
                for character in subpart:
                    value = 0 if character == "n" else int(character)
                    assert value >= 0 and value <= 9
                    if character == "n":
                        self.cells.append(0)
                    else:
                        self.cells.append(int(character))
        assert len(self.cells) == 81
        assert self._satisfies_all_constraints()

        def _get_unknown_indexes() -> List[Index]:
            # return the list of unknown indexes (i.e. with value == 0)
            indexes = []
            for row in range(9):
                for col in range(9):
                    index = Index(row, col)
                    if self._get(index) == 0:
                        indexes.append(index)
            return indexes

        self.unknown_indexes = _get_unknown_indexes()
        # on grille_d it was faster to go in reverse
        self.unknown_indexes.reverse()

    def solve(self):
        print(f"Starting to solve: {len(self.unknown_indexes)} unknowns")
        return self._solve_recursively()

    def print(self):
        limite=[3,6,9]
        for i in range(0,9): 
            lig_temp=""
            if i in limite: 
                print("------ ------- ------")
            for j in range(0,9):
                if j in limite:
                    lig_temp+='|'+' '
                lig_temp+=str(self.cells[j+i*9])+' '
            print(lig_temp)
                        
    def _get(self, index: Index) -> int:
        return self.cells[index.row * 9 + index.col]
    
    def _set(self, index: Index, value: int) -> None:
        self.cells[index.row * 9 + index.col] = value

    def _satisfies_constraint(self, constraint: Constraint) -> bool:
        values_seen = set()
        for index in constraint.indexes:
            value = self._get(index)
            if value == 0:
                continue
            if value in values_seen:
                return False
            else:
                values_seen.add(value)
        return True
    
    def _satisfies_constraints_of_index(self, index: Index):
        for constraint in SudokuMatrix.constraints_by_index[index]:
            if not self._satisfies_constraint(constraint):
                return False
        return True
    
    def _satisfies_all_constraints(self):
        for constraint in SudokuMatrix.all_constraints:
            if not self._satisfies_constraint(constraint):
                return False
        return True

    def _solve_recursively(self):
        # Returns true if sudoku is solvable. Try all possibilities recursively.
        if len(self.unknown_indexes) == 0:
            return True
        index = self.unknown_indexes.pop()
        value = self._get(index)
        assert value == 0
        # try naively all possibilities
        # TODO(Mathias) we could try reducing the search space if we precompute it and update it
        for possible_value in range(1, 10):
            self._set(index, possible_value)
            if self._satisfies_constraints_of_index(index) and self._solve_recursively():
                return True
            # else we try other values

        # if all possible values fail, we reset to 0.
        self._set(index, 0)
        self.unknown_indexes.append(index)
        return False



if __name__ == "__main__":

    grille_ttf=[["712 384 965"],
                ["856 917 432"],
                ["nnn 652 871"],
                ["nnn nnn 657"],
                ["671 nnn 243"],
                ["nnn nnn 189"],
                ["nnn nnn 594"],
                ["489 571 3nn"],
                ["325 469 718"]]
	
    grille_tf= [["712 384 965"],["n56 917 n3n"],["n4n n5n 871"], 
                ["n9n nnn 6nn"],["671 n95 2nn"],["nnn n2n 1nn"], 
                ["167 n3n 59n"],["48n n7n 3nn"],["n25 46n nnn"]]
	
    grille_f=  [["nn2 n8n n6n"],["n56 917 n3n"],["n4n n5n 871"],
                ["n9n nnn 6nn"],["671 n95 2nn"],["nnn n2n 1nn"], 
                ["167 n3n 59n"],["48n n7n 3nn"],["n25 46n nnn"]] 

    grille_d=  [["nnn 4nn nnn"],["8nn nn3 5n2"],["n3n n9n n6n"],
                ["15n n8n nnn"],["4nn n1n 3nn"],["2nn n3n 78n"],
                ["nnn nnn n3n"],["nnn n7n 29n"],["nnn 1nn nn6"]]

    # This is a minimal sudoku grid that we don't solve in a reasonable amount of time...
    sudoku_17clues=[["nnn nnn n1n"],
                    ["nnn nn2 nn3"],
                    ["nnn 4nn nnn"],
                    ["nnn nnn 5nn"],
                    ["4n1 6nn nnn"],
                    ["nn7 1nn nnn"],
                    ["n5n nnn 2nn"],
                    ["nnn n8n n4n"],
                    ["n3n 91n nnn"]]
    			
    sudoku_matrix = SudokuMatrix(grille_d)
    print("Initial grid (0 are unknowns):")
    sudoku_matrix.print()
    start = time.time()
    try:
        print(f"Solvable: {sudoku_matrix.solve()}")
        print(f"solved in {time.time() - start} seconds")
    except KeyboardInterrupt:
        print(f"Cancelled solving after {time.time() - start} seconds")
    print("final state:")
    sudoku_matrix.print()