import time

class Case: 
 
	def __init__(self,x,y,val,pos): 
		self.x = x 
		self.y = y 
		self.val = val 
		self.pos = pos 

	def affiche_grille(vecteur):
		limite=[3,6,9] 
		for i in range(0,9): 
			lig_temp=""
			if i in limite: 
				print("------ ------- ------")
			for j in range(0,9):
				if j in limite:
					lig_temp+='|'+' '
				lig_temp+=str(vecteur[j+i*9])+' '
			print(lig_temp)
	 
	def construct_case(vecteur_grille): 
		global x, y, val, pos 

		x=1 #abscisse 
		y=1 #ordonnée 
		for element_vecteur in vecteur_grille:
			if x == 10: 
				x = 1
				y+=1 
			val="n" 
			pos=[]
			if x==10: 
				x=1
			if element_vecteur not in ["[","]","'"] : 
				if element_vecteur =="n" : 
					pos=[1,2,3,4,5,6,7,8,9] 
				else:
					val=element_vecteur 
				exec("case{}{} = Case(x,y,val,pos)".format(str(x),str(y)),globals())
			x+=1 

	def maj_pos_case(list_lig,list_col,list_car): 
		global val_impos 
		for i in range (1,10): #on balaye toute les lignes
			for j in range (1,10): #maintenant on balaye les colonnes
				val_impos=[] 
				exec("val=case{}{}.val".format(j,i),  globals()) 
				if val == "n": 
					for it in list_col[j-1]:
						if it != "n":
							val_impos.append(int(it)) 
					for it in list_lig[i-1]:
						if it != "n":
							val_impos.append(int(it))
					if   j<4  and i<4:  num_car=1 
					elif j<7  and i<4:  num_car=2 
					elif j<10 and i<4:  num_car=3 
					elif j<4  and i<7:  num_car=4 
					elif j<7  and i<7:  num_car=5 
					elif j<10 and i<7:  num_car=6 
					elif j<4  and i<10: num_car=7 
					elif j<7  and i<10: num_car=8 
					elif j<10 and i<10: num_car=9 
					for it in list_car[num_car-1]:
						if it != "n":
							val_impos.append(int(it)) 
					val_impos.sort() 
					#suppression des doublon de la liste valeur impossible 
					temp_li = []  
					for it in val_impos :  
						if it not in temp_li:  
							temp_li.append(it)  
					val_impos=temp_li.copy() 
					exec("pos=case{}{}.pos.copy()".format(j,i), globals())
  
					for it_impos in val_impos: 
						if it_impos in pos: 
							pos.remove(it_impos)
					exec("case{}{}.pos=pos.copy()".format(j,i), globals())
					#print(j,i,"POS : ",pos,"IMPOS : ",val_impos) 

	def maj_val_case(): 
		flag_modif = False 
		for i in range (1,10): 
			for j in range (1,10): 
				exec("pos=case{}{}.pos".format(j,i), globals()) 
				exec("val=case{}{}.val".format(j,i), globals()) 
				if len(pos) == 1 and val =="n": 
					exec("case{}{}.val=str(pos[0])".format(j,i), globals()) 
					flag_modif = True 
		return flag_modif 
	pass 

class Col: 
	def __init__(self,num): 
		self.nom="col"+str(num) 
	 
	def creation_col(vecteur_entree):
		compt_temp=0
		list_col=[]
		col_temp=[]
		for j in range (0,9):
			for i in range (0,9):
				col_temp.append(vecteur_entree[j+i*9])
			list_col.append(col_temp)
			col_temp=[]
		return list_col
	 
	def verif_col(list_col): 
		flag = True 
		for k in range(0,9): 
			for it_i in list_col[k]: 
				li_temp=list_col[k].copy() 
				li_temp.remove(it_i) 
				for it_j in li_temp: 
					if it_i==it_j: 
						flag = False
						break
		return flag 
		 
	pass 
  
class Car: 
	def __init__(self,case1,case2,case3,case4,case5,case6,case7,case8,case9): 
		self.case1=case1 
		self.case2=case2 
		self.case3=case3 
		self.case4=case4 
		self.case5=case5 
		self.case6=case6 
		self.case7=case7 
		self.case8=case8 
		self.case9=case9 
	

	def verif_car(list_car):
		flag = True ; li_temp=[]
		for k in range(0,9):
			for it_i in list_car[k]: 
				li_temp=list_car[k].copy() 
				li_temp.remove(it_i) 
				for it_j in li_temp: 
					if it_i==it_j: 
						flag = False
						break
		return flag

	def creation_car(vecteur_entree):
		list_car=[]
		j=0
		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]

		for i in range(0,27):
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vecteur_entree[i]) ; j+=1
			elif j<6 : car_temp2.append(vecteur_entree[i]) ; j+=1
			elif j<9 : car_temp3.append(vecteur_entree[i]) ; j+=1
		list_car.append(car_temp1)
		list_car.append(car_temp2)
		list_car.append(car_temp3)

		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]
		for i in range(27,54): 
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vecteur_entree[i]) ; j+=1
			elif j<6 : car_temp2.append(vecteur_entree[i]) ; j+=1
			elif j<9 : car_temp3.append(vecteur_entree[i]) ; j+=1
		list_car.append(car_temp1)
		list_car.append(car_temp2)
		list_car.append(car_temp3)

		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]

		for i in range(54,81): 
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vecteur_entree[i]) ; j+=1
			elif j<6 : car_temp2.append(vecteur_entree[i]) ; j+=1
			elif j<9 : car_temp3.append(vecteur_entree[i]) ; j+=1
		list_car.append(car_temp1)
		list_car.append(car_temp2)
		list_car.append(car_temp3)

		return list_car
	 
	pass 
  
class Lig: 
	def __init__(self,num): 
		self.nom="lig"+str(num) 
		 
	def creation_lig(vecteur_entree):
		lig_temp=[]
		list_lig=[]
		compt_temp=0
		for element_vecteur in vecteur_entree:
			lig_temp.append(element_vecteur)
			compt_temp+=1
			if compt_temp ==9:
				compt_temp=0
				list_lig.append(lig_temp)
				lig_temp=[]
		return list_lig
	 
	def verif_lig(temp_lig): 
		flag = True 
		for it_i in temp_lig: 
			li_temp=temp_lig.copy() 
			li_temp.remove(it_i) 
			for it_j in li_temp: 
				if it_i==it_j: 
					flag = False
					break
		return flag 
	pass 

class Vecteur:
	def __init__(self): 
		self.val=[]
		
	def lecture_grille(grille):
		vecteur=[]
		for ligne_grille in grille:
			for element_ligne in str(ligne_grille):
				if element_ligne not in [" ","'","[","]"]:
					vecteur.append(element_ligne)
		return vecteur

	def construct_vect_pos():
		vect_pos=[]
		#vect_pos cotient toute les possibilités pour haque cases. C'est un vecteur de vecteur : chaque itération stocke uniquement les valeurs possibles
		#vect_pos_compt est un vecteur de vecteur : en position 1 il stocke le nombre de possibilité, en position 2 il stocke là ou en est le compteur [long, compt]
		vect_pos_compt=[]
		for i in range (1,10): 
			for j in range (1,10): 
				exec("val_temp=case{}{}.val".format(j,i),globals()) 
				if val_temp != "n": 
					vect_pos.append([int(val_temp)]) 
					vect_pos_compt.append([1,0]) 
				else: 
					exec("pos_temp=case{}{}.pos".format(j,i),globals()) 
					vect_pos.append(pos_temp)
					vect_pos_compt.append([len(pos_temp),0])
		return vect_pos,vect_pos_compt

	def construct_lig_test(debut, vecteur_entree,compteur):
		lig_test=[]
		for i in range(debut,debut+9):
			compt_temp=compteur[i][1] 
			lig_test.append(vecteur_entree[i][compt_temp])
		return lig_test

	def construct_vect_test(vect_entree,compteur):
		vect_test=[]
		vect_test=[0]*81
		for i in range(80,-1,-1): 
			compt_temp=compteur[i][1] 
			vect_test[i] = vect_entree[i][compt_temp]
		return vect_test

	def maj_vect_compt(compteur):
		compteur[80][1]+=1
		for i in range (80,-1,-1):
			if compteur[i][1] == compteur[i][0]:
					compteur[i][1]=0
					compteur[i-1][1]+=1
			else:
				break
		return compteur

def main():

	start_time = time.time()

	vect_grille=[]
	vect_grille=Vecteur.lecture_grille(grille)
	#Constructions des cases 
	Case.construct_case(vect_grille)
	#Constructions des lignes 
	list_lig=Lig.creation_lig(vect_grille)
	#Constructions des colonnes 
	list_col=Col.creation_col(vect_grille)
	#Constructions des carrées 
	list_car=Car.creation_car(vect_grille)

	flag_modif =True 
	m=0

	while flag_modif==True:
		
		#balayage de chaque case pour savoir s'il faut mettre à jour les possibilités
		Case.maj_pos_case(list_lig,list_col,list_car)
		#balayage de toutes les cases pour mettre à jour les valeur si il ne reste qu'une seule possibilité
		flag_modif=Case.maj_val_case()

		#Forcer le nombre d'itérations
		if nbr_it_premier_cycle!=0:
			print("Mode forçage itération n° : ",m)
			flag_modif=True
			if m==nbr_it_premier_cycle:
				break
		m+=1

	print("Nombre d'itérations etape 1 : ",m) 

	Case.affiche_grille(vect_grille)  
	
	#on construit ici le vecteur qui stocke les valeurs possible. ce vecteur est construit à partir des valeurs stockées dans les case sous la forme casexy.pos
	vect_pos,vect_pos_compt=Vecteur.construct_vect_pos()

	nbr_combinaison=1 

	#Calcul nombre de possibilité
	for i in range(0,81): 
		nbr_combinaison*=len(vect_pos[i]) 

	end_time_etape1 = time.time()

	print("Durée étape 1 : ",end_time_etape1-start_time)

	print("Nombre de possibilité : ",nbr_combinaison)

	flag_col_bon = False; flag_lig_bon = False; flag_car_bon = False 

	#Boucle d'itération pour tester toute les solutions possibles
	h=0 ; g=0

	debut_boucle = time.time()

	lig_test=[0]*9

	while flag_car_bon == False:
		h+=1 ; g+=1

		flag_lig_bon = True
		flag_seconde_verif = True

		for num_lig in range(0,9):
			#création de la ligne à tester
			lig_test[num_lig]=Vecteur.construct_lig_test(debut=num_lig*9,vecteur_entree=vect_pos,compteur=vect_pos_compt)
			#1+2+3+4+5+6+7+8+9 = 45
			if sum(lig_test[num_lig])!=45:
				flag_lig_bon=False
				flag_seconde_verif=False
				break

		if flag_seconde_verif==True:
			print("SECONDE VERIF")

			for num_lig in range(0,9):
				#Vérification de présence du chiffre dans la ligne
				flag_lig_bon = Lig.verif_lig(lig_test[num_lig])

				if flag_lig_bon == False:
					break
			
		if flag_lig_bon == True:
			#creation du vecteur à tester
			vect_test=Vecteur.construct_vect_test(vect_pos,vect_pos_compt)
			print("ligne OK")
			flag_lig_bon = False 
			#Création de la liste des colonnes de la grille qui doit être tester
			list_col_test=Col.creation_col(vect_test)
			#Vérification de présence du chiffre dans la colonne 
			flag_col_bon=Col.verif_col(list_col_test)
			
			if flag_col_bon == True:
				print("colonne OK")			
				flag_col_bon = False 
				#Création de la liste des carrés de la grille qui doit être tester
				list_car_test=Car.creation_car(vect_test)
				#Vérification de présence du chiffre dans le carré
				flag_car_bon=Car.verif_car(list_car_test)
				print("flag_car_bon",flag_car_bon)

		if h==100000:
			temp_time=1.01
			temp_time=(time.time()-debut_boucle)
			print("Durée moyenne 100 000 itération : ",temp_time)
			print("Temps Maximum estimé : ",temp_time*nbr_combinaison*0.00001)
		
		if g ==1000000:
			print("Avancement : ",h/nbr_combinaison*100," %")
			g=0

		#décalage du compteur de 1
		vect_pos_compt=Vecteur.maj_vect_compt(vect_pos_compt)

	print("Nombre d'itérations : ",h) 
	Case.affiche_grille(vect_test) 	

if __name__ == "__main__":

	grille_ttf=[["712 384 965"],["856 917 432"],["nnn 652 871"],
				["nnn nnn 657"],["671 nnn 243"],["nnn nnn 189"],
				["nnn nnn 594"],["489 571 3nn"],["325 469 718"]]
	
	grille_tf= [["712 384 965"],["n56 917 n3n"],["n4n n5n 871"], 
				["n9n nnn 6nn"],["671 n95 2nn"],["nnn n2n 1nn"], 
				["167 n3n 59n"],["48n n7n 3nn"],["n25 46n nnn"]]
	
	grille_f=  [["nn2 n8n n6n"],["n56 917 n3n"],["n4n n5n 871"],
				["n9n nnn 6nn"],["671 n95 2nn"],["nnn n2n 1nn"], 
				["167 n3n 59n"],["48n n7n 3nn"],["n25 46n nnn"]] 

	grille_d=  [["nnn 4nn nnn"],["8nn nn3 5n2"],["n3n n9n n6n"],
				["15n n8n nnn"],["4nn n1n 3nn"],["2nn n3n 78n"],
				["nnn nnn n3n"],["nnn n7n 29n"],["nnn 1nn nn6"]]
				
	grille=grille_tf.copy()
	nbr_it_premier_cycle = 0
	main()