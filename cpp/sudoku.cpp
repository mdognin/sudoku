#include <iostream>
#include <list>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <numeric>

using namespace std;

//parcours liste for (int x : my_list) OU for (int j = 0; j < vect[i].size(); j++)
//to_string(int) : convertir int en string
//vector_n.append(element) : équivalent de append pour les vecteur
//afficher un vecteur copy( vec.begin(), vec.end(), output );  cout<<endl;
//int num = stoi(str); convertir un string en int
//afficher un vecteur entier vector<int> vecteur_cible = xxxxxxxxxxx ;  for (int i=0; i<vecteur_cible.size(); i++){cout << vecteur_cible[i];}
//copier un vecteur for (int i=0; i<vect1.size(); i++)    vect2.push_back(vect1[i]);
// enlever un élément d'un vecteur remove(v.begin(),v.end(),val)
//faire une pause dans le programme et attendre que l'utilisateur appuie sur "entrée" : std::cin.get();

bool contains_int(vector<int> &list, int x){
	return find(list.begin(), list.end(), x) != list.end();
}

bool not_contains_str(vector<string> &list, string x) {
    bool flag;
	flag= find(list.begin(), list.end(), x) != list.end();
	if (flag==true)
    {
        return false;
    }
    else{return true;}
}

bool not_contains_int(vector<int> &list, int x) {
    bool flag;
	flag= find(list.begin(), list.end(), x) != list.end();
	if (flag==true)
    {
        return false;
    }
    else{return true;}
}

void affiche_grille(vector<int> vect_grille) {
    vector<int> limite = {3,6};
    string lig_temp ;
    for (int i=0 ; i<9 ; i++)
    {
        lig_temp=" ";
        if ( contains_int(limite,i) )
        {
            cout <<"------  -------  ------"<<endl;
        }
        for (int j=0;j<9;j++)
        {
            if (contains_int(limite,j))
                {
                    lig_temp +='|' ;
                    lig_temp +=" " ;
                }
                lig_temp += to_string(vect_grille[j+i*9]);
                lig_temp += " ";
        }
        cout<< lig_temp<<endl;
    }
}

vector<int> copie_vect(vector<int> entree){
    vector<int> temp_vect ={};
    vector<int> sortie ={};
    //Copie de vecteur
    for (int i=0; i<entree.size(); i++) {
        sortie.push_back(entree[i]);
    }
    return sortie;
}

vector<int> enleve_valeur(vector<int>vecteur,int valeur){
    vector<int> vecteur_2;
    int curseur;

    vecteur_2 = copie_vect(vecteur);
    for (int it=0;it<9;it++) {if (vecteur[it]==valeur) {curseur=it;} }
    vecteur_2.erase(vecteur_2.begin() + curseur);
    return vecteur_2;
}// enlever un élément d'un vecteur


void affiche_vect(vector<int> entree){
    for (int i=0; i<entree.size(); i++){cout << entree[i];}
    cout<<endl;
}

void affiche_db_vect(vector<vector<int>> entree){
    for (int j=0; j<entree.size();j++) {

        for (int i=0; i<entree[j].size(); i++){cout << entree[j][i];}
        //cout<<endl;
    }

    cout<<endl;
}

vector<int> lecture_grille(vector<string> grille){
		vector<int> vect_int;
		vector<char> vect_char;
		for (string ligne_grille : grille) {
            for (int j = 0; j < ligne_grille.size(); j++) {
                if (ligne_grille[j]!=' ')
                {
                    if (ligne_grille[j]=='n') {
                        vect_char.push_back('0');
                    }
                    else {
                        vect_char.push_back(ligne_grille[j]);
                    }
                }
            }
        }
        for (int j = 0; j < vect_char.size(); j++){
            string temp="";
            int temp_int;
            temp+=vect_char[j];
            temp_int=stoi(temp);
            vect_int.push_back(temp_int);
        }
        return vect_int;

    }

vector<vector<vector<int>>> construct_case(vector<int> vecteur_grille){

    vector<vector<vector<int>>> cases;
    vector<vector<int>> vect_temp;

    for (int i=0; i<81 ; i++){
        vect_temp.clear();

        if (vecteur_grille[i]==0)
        {
            vect_temp.push_back({0});
            vect_temp.push_back({1,2,3,4,5,6,7,8,9});
        }
        else
        {
            vect_temp.push_back({vecteur_grille[i]});
            vect_temp.push_back({0});
        }

        cases.push_back(vect_temp);

    }
    return cases;

}

vector<vector<int>> creation_lig(vector<int> vecteur_grille){
    vector<int> lig_temp={};
    vector<vector<int>> list_lig={};
    int compt_temp=0;

    for (int i=0; i<vecteur_grille.size(); i++){
        lig_temp.push_back(vecteur_grille[i]);
        compt_temp+=1;
        if (compt_temp==9){
            compt_temp=0;
            list_lig.push_back(lig_temp);
            lig_temp={};
        }
    }
    return list_lig;
}

vector<vector<int>> creation_col(vector<int> vecteur_grille){
    vector<int> col_temp={};
    vector<vector<int>> list_col={};
    int compt_temp=0;

    for (int j=0; j<9; j++){
        for (int i=0; i<9; i++){
            col_temp.push_back(vecteur_grille[j+i*9]);
        }
        list_col.push_back(col_temp);
        col_temp={};
    }
    return list_col;
}

vector<vector<int>> creation_car(vector<int> vecteur_grille){
    vector<vector<int>> list_car;
    int j=0;

    //Première ligne de carrées
    vector<int> car_temp1={};
    vector<int> car_temp2={};
    vector<int> car_temp3={};
    for (int i=0; i< 27 ;i++){
        if (j==9) {j=0;}
        if (j<3)      {car_temp1.push_back(vecteur_grille[i]); j +=1; }
        else if (j<6) {car_temp2.push_back(vecteur_grille[i]); j +=1; }
        else if (j<9) {car_temp3.push_back(vecteur_grille[i]); j +=1; }
    }
    list_car.push_back(car_temp1);
    list_car.push_back(car_temp2);
    list_car.push_back(car_temp3);

    //Seconde ligne de carrées
    car_temp1={};
    car_temp2={};
    car_temp3={};
    for (int i=27; i< 54 ;i++){
        if (j==9) {j=0;}
        if (j<3)      {car_temp1.push_back(vecteur_grille[i]); j +=1; }
        else if (j<6) {car_temp2.push_back(vecteur_grille[i]); j +=1; }
        else if (j<9) {car_temp3.push_back(vecteur_grille[i]); j +=1; }
    }
    list_car.push_back(car_temp1);
    list_car.push_back(car_temp2);
    list_car.push_back(car_temp3);

    //troisième ligne de carrées
    car_temp1={};
    car_temp2={};
    car_temp3={};
    for (int i=54; i< 81 ;i++){
        if (j==9) {j=0;}
        if (j<3)      {car_temp1.push_back(vecteur_grille[i]); j +=1; }
        else if (j<6) {car_temp2.push_back(vecteur_grille[i]); j +=1; }
        else if (j<9) {car_temp3.push_back(vecteur_grille[i]); j +=1; }
    }
    list_car.push_back(car_temp1);
    list_car.push_back(car_temp2);
    list_car.push_back(car_temp3);

    return list_car;

}

vector<vector<vector<int>>> maj_pos_case(vector<vector<vector<int>>> cases, vector<vector<int>> list_lig, vector<vector<int>> list_col,vector<vector<int>> list_car){
    vector<int> valeur_impos; //valeur impossible
    int val;
    //ici on va utiliser une grille de dimension 9*9 pluto qu'un vecteur de taille 81*1
    vector<vector<int>> grille_val;
    vector<int> pos;
    vector<vector<vector<int>>> grille_pos;
    vector<int> temp_vect;
    int lig=0;
    int col=0;
    int lig2=0;
    int col2=0;
    vector<int> vect_temp;

    for (int i=0;i<81;i++){
        vect_temp.push_back(cases[i][0][0]);
        col+=1;
        if(col==9){
            grille_val.push_back(vect_temp);
            vect_temp={};
            lig+=1;
            col=0;
        }
    }

    lig=0;
    col=0;
    for (lig=0;lig<9;lig++) {

        for (col=0;col<9;col++) {

            valeur_impos = {};
            val=grille_val[lig][col];

            if (val==0){
                //On rajoute les valeur impossible de la colonne
                for (int i=0; i<list_col[col].size(); i++){
                    valeur_impos.push_back(list_col[col][i]);
                }
                //On rajoute les valeur impossible de la ligne
                for (int i=0; i<list_lig[lig].size(); i++){
                    valeur_impos.push_back(list_lig[lig][i]);
                }
                int num_car;
                if  (col<3  && lig<3){ num_car=0 ;}
                else if (col<6  && lig<3) { num_car=1; }
                else if (col<9 && lig<3) { num_car=2; }
                else if (col<3  && lig<6) { num_car=3; }
                else if (col<6  && lig<6) { num_car=4; }
                else if (col<9 && lig<6) { num_car=5; }
                else if (col<3  && lig<9) { num_car=6; }
                else if (col<6  && lig<9) { num_car=7; }
                else if (col<9 && lig<9) { num_car=8; }


                //On rajoute les valeur impossible du carrée
                for (int i=0; i<list_car[num_car].size(); i++){
                    valeur_impos.push_back(list_car[num_car][i]);
                }
                //Classement du vecteur valeur_impos
                sort(valeur_impos.begin(), valeur_impos.end());

                //suppression des doublon de la liste valeur impossible
                vector<int> temp_li;
                for (int i=0; i<valeur_impos.size(); i++){
                    if (not_contains_int(temp_li,valeur_impos[i])) {
                        temp_li.push_back(valeur_impos[i]);
                    }
                }

                valeur_impos.clear();
                valeur_impos=copie_vect(temp_li);


                lig2=0;
                col2=0;

                vector<vector<int>> vect_lig_temp;
                for (int i=0;i<81;i++){
                    vect_temp={};
                    for (int j=0; j<cases[i][1].size(); j++) {
                        vect_temp.push_back(cases[i][1][j]);
                    }
                    vect_lig_temp.push_back(vect_temp);
                    col2+=1 ;
                    if(col2==9) {
                        lig2+=1;
                        col2=0;
                        grille_pos.push_back(vect_lig_temp);


                        vect_lig_temp={};
                    }
                }

                //Récupération de la liste des valeur possible pour la case actuelle dans la grille_pos
                pos.clear();
                pos=copie_vect(grille_pos[lig][col]);

                //On met à jour le vecteur des valeurs possibles de la case à partir la liste des valeurs impossible de la case
                for (int i=0; i<valeur_impos.size(); i++){
                    if (contains_int(temp_li, valeur_impos[i]) ) {
                        remove(pos.begin(),pos.end(),valeur_impos[i]) ;
                    }
                }

                //suppression des doublon du vecteur des valeurs possible
                temp_vect={};
                for (int i=0; i<pos.size(); i++){
                    if (not_contains_int(temp_vect,pos[i])) {
                        temp_vect.push_back(pos[i]);
                    }
                }

                pos.clear();
                pos=copie_vect(temp_vect);

                grille_pos[lig][col].clear();
                grille_pos[lig][col]=copie_vect(pos);

            }
        }
    }


    int compt=0;
    int hist_lig3=0;
    for (int lig3=0;lig3<9;lig3++) {
        for (int col3=0;col3<9;col3 ++) {
            if (cases[compt][0][0]==0){
                cases[compt][1].clear();
                cases[compt][1]=copie_vect(grille_pos[lig3][col3]);
            }
            compt+=1;
        }
    }
    return cases;
}

pair<bool,vector<vector<vector<int>>> > maj_val_case(vector<vector<vector<int>>> cases) {

    pair<bool,vector<vector<vector<int>>> > resultat;
    bool flag_modif = false ;
    vector<int> pos;
    int val;

    for (int i=0;i<81;i++) {
        pos=copie_vect(cases[i][1]);
        val = cases[i][0][0];
        if (pos.size() ==1 and val ==0){
            cases[i][0][0]=pos[0];
            flag_modif = true;
        }
    }
    resultat.first=flag_modif;
    resultat.second=cases;
    return resultat;
}

pair< vector<vector<int>>, vector<vector<int>> > construct_vect_pos(vector<vector<vector<int>>> cases) {
    pair< vector<vector<int>>, vector<vector<int>> > resultat;
    vector<vector<int>> vect_pos ;
    vector<vector<int>> vect_pos_compt;
    vector<int> pos_temp;
    int val_temp;

    for (int i=0;i<81;i++) {
        val_temp=cases[i][0][0];
        if (val_temp != 0) {
            vect_pos.push_back({val_temp});
            vect_pos_compt.push_back({1,0});
        }
        else{
            pos_temp=copie_vect(cases[i][1]);
            vect_pos.push_back(pos_temp);
            vect_pos_compt.push_back({pos_temp.size(),0});
        }
    }
    resultat.first=vect_pos;
    resultat.second=vect_pos_compt;
    return resultat;
}

vector<int> construct_lig_test (int debut, vector<vector<int>> vecteur_entree, vector<vector<int>> compteur){

    vector<int> lig_test;
    int compt_temp;
    for (int i=debut ; i<debut+9 ; i++) {
        compt_temp=compteur[i][1];
        lig_test.push_back(vecteur_entree[i][compt_temp]);
    }
    return lig_test;
}

bool verif_lig (vector<int> vect_ligne) {
    bool flag = true ;
    vector<int> temp_ligne;
    cout<<"vect_ligne ";
    affiche_vect(vect_ligne);
    for (int i =0; i<vect_ligne.size() ;i++ ) {
        temp_ligne=copie_vect(vect_ligne);
        // enlever un élément d'un vecteur
        temp_ligne=enleve_valeur(temp_ligne,vect_ligne[i]);

        for (int j=0; j<temp_ligne.size() ; j++) {
            if (vect_ligne[i] == temp_ligne[j] ) {
                cout<<"vect_ligne[i] "<<vect_ligne[i]<<"temp_ligne[j] "<<temp_ligne[j]<<endl;
                flag = false;
                break;
            }
        }
    }

    return flag ;
}

bool verif_col (vector<vector<int>> vvect_col ) {
    bool flag = true ;
    vector<int> temp_col;
    for (int k=0; k<9 ; k++) {
        for (int i =0; i<vvect_col[k].size() ;i++ ) {
            temp_col=copie_vect(vvect_col[k]);
            // enlever un élément d'un vecteur
            temp_col=enleve_valeur(temp_col,vvect_col[k][i]);

            for (int j=0; j<temp_col.size() ; j++) {
                if (vvect_col[k][i] ==temp_col[j] ) {
                    flag = false;
                    break;
                }
            }
        }
    }
    return flag ;
}

vector<int> construct_vect_test (vector<vector<int>>vect_entree, vector<vector<int>>compteur) {

    vector<int> vect_test(81,0);
    affiche_vect(vect_test);
    int compt_temp ;
    for (int i=80; i>-1 ; i--) {
        compt_temp=compteur[i][1] ;
        vect_test[i] = vect_entree[i][compt_temp] ;
    }

    return vect_test;
}

vector<vector<int>> maj_vect_compt(vector<vector<int>> compteur) {
    compteur[80][1]+=1;

    for (int i=80; i>-1;i--){
        if (compteur[i][1] == compteur[i][0]) {
            compteur[i][1]=0;
            compteur[i-1][1]+=1;
        }
        else{
            break;
        }
    }
    return compteur ;
}


int main() {
    vector<string> grille,grille1,grille2;

    grille1={"712 384 965","856 917 432","n4n 652 871",
            "n9n 143 657","671 n95 243","nnn n2n 189",
            "167 238 594","489 571 326","325 469 718"};
    grille2={"712 n8n n6n","n56 917 n3n","n4n n5n 871",
             "n9n 143 657","671 n95 243","nnn n2n 1nn",
             "167 n3n 594","48n n7n 326","n25 46n 718"};


    grille=grille2;

    vector<int> vecteur_grille;

    vecteur_grille=lecture_grille(grille);
    affiche_grille(vecteur_grille);

    //Constructions des cases
    vector<vector<vector<int>>> cases;
    cases=construct_case(vecteur_grille);

     //Constructions des lignes
    vector<vector<int>> list_lig;
	list_lig=creation_lig(vecteur_grille);

	//Constructions des colonnes
	vector<vector<int>> list_col;
	list_col=creation_col(vecteur_grille);

	//Constructions des carrées
	vector<vector<int>> list_car;
	list_car=creation_car(vecteur_grille);

	bool flag_modif = true;
	int m =0 ;

	while (flag_modif==true){

        //balayage de chaque case pour savoir s'il faut mettre à jour les possibilités

		cases=maj_pos_case(cases,list_lig,list_col,list_car);

 		//balayage de toutes les cases pour mettre à jour les valeur si il ne reste qu'une seule possibilité
		flag_modif=maj_val_case(cases).first;
		cases=maj_val_case(cases).second;


		//Forcer le nombre d'itérations
		/*if nbr_it_premier_cycle!=0:
			print("Mode forçage itération n° : ",m)
			flag_modif=True
			if m==nbr_it_premier_cycle:
				break*/
		m+=1;
		flag_modif=false;
	}
    cout<<endl<<"Nombre d'itérations etape 1 : "<<m<<endl;

    vector<int> vecteur_temp={};
    for (int i=0;i<81;i++) {
        vecteur_temp.push_back(cases[i][0][0]);
    }
	affiche_grille(vecteur_temp);

    //on construit ici le vecteur qui stocke les valeurs possible. ce vecteur est construit à partir des valeurs stockées dans les case sous la forme casexy.pos
	vector<vector<int>> vect_pos, vect_pos_compt;
	vect_pos=construct_vect_pos(cases).first;
	vect_pos_compt=construct_vect_pos(cases).second;

	//Calcul nombre de possibilité
	long long int nbr_combinaison=1;
	for (int i=0; i<81; i++) {
        nbr_combinaison*=vect_pos[i].size();
	}
	cout<<endl<< "Nombre de possibilité : " << nbr_combinaison<<endl;

	//Boucle d'itération pour tester toute les solutions possibles
	bool flag_col_bon = false;
	bool flag_lig_bon = false;
	bool flag_car_bon = false ;
	int h=0 ;
	int g=0 ;
	vector<vector<int>> lig_test ;
	int somme_vect;
	vector<int> vect_test;
    bool flag_seconde_verif ;
    long double calcul;

	while (flag_car_bon == false) {
	    h+=1 ; g+=1;
	    flag_lig_bon = true ;
	    flag_seconde_verif = true ;

        lig_test.clear();
	    for (int num_lig=0; num_lig<9 ; num_lig++){

            //création de la ligne à tester
			lig_test.push_back(construct_lig_test(num_lig*9,vect_pos,vect_pos_compt));

            //1+2+3+4+5+6+7+8+9 = 45
			somme_vect=0;
			for( int i=0; i<lig_test[num_lig].size() ; i++) {
                somme_vect +=lig_test[num_lig][i];
			}

			if (somme_vect!=45) {
                flag_lig_bon=false;
				flag_seconde_verif=false;
				break;
			}
	    }

	    if (flag_seconde_verif==true) {
            for (int num_lig = 0; num_lig < 9; num_lig++){
                //Vérification de présence du chiffre dans la ligne
				flag_lig_bon = verif_lig(lig_test[num_lig]);
				if (flag_lig_bon == false){
                    break;
				}
            }
	    }

	    if (flag_lig_bon == true) {
            //creation du vecteur à tester

            vector<vector<int>> list_col_test;
			vect_test=construct_vect_test(vect_pos,vect_pos_compt);
			affiche_vect(vect_test);
			cout<<endl<<"ligne OK"<<endl;
			flag_lig_bon = false ;
			//Création de la liste des colonnes de la grille qui doit être tester
			list_col_test=creation_col(vect_test);
			//Vérification de présence du chiffre dans la colonne
			flag_col_bon=verif_col(list_col_test);
			cout<<flag_col_bon;
			if (flag_col_bon == true) {
                vector<vector<int>> list_car_test;
				cout<<endl<<"colonne OK"<<endl;
				flag_col_bon = false;
				//Création de la liste des carrés de la grille qui doit être tester
				list_car_test=creation_car(vect_test);
				//Vérification de présence du chiffre dans le carré
				flag_car_bon=verif_col(list_car_test);
				cout<<endl<<"flag_car_bon "<<flag_car_bon<<endl;
			}
	    }
	    if (h==100000) {
                cout<<"temps"<<endl;
            //print("Durée moyenne 100 000 itération : ",temp_time)
			//("Temps Maximum estimé : ",temp_time*nbr_combinaison*0.00001)
	    }
		if (g ==100000) {
            calcul = h/nbr_combinaison*100;
            cout<<"Avancement : "<<calcul<<" % "<<h<<endl;
			g=0;
		}

		//décalage du compteur de 1
		vect_pos_compt=maj_vect_compt(vect_pos_compt);
	}
	affiche_grille(vect_test);

    return 0;
}
