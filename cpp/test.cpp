#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <array>
#include <sstream>
#include <algorithm>

#include <iterator>
#include <stdexcept>
using namespace std;

vector<int> copie_vect(vector<int> entree){
    vector<int> temp_vect ={};
    vector<int> sortie ={};
    //Copie de vecteur
    for (int i=0; i<entree.size(); i++) {
        sortie.push_back(entree[i]);
    }
    return sortie;
}

void affiche_vect(vector<int> entree){
    for (int i=0; i<entree.size(); i++){cout << entree[i];}
    cout<<endl;
}

vector<int> enleve_valeur(vector<int>vecteur,int valeur){
    vector<int> vecteur_2;
    int curseur;

    vecteur_2 = copie_vect(vecteur);
    for (int it=0;it<9;it++) {if (vecteur[it]==valeur) {curseur=it;} }
    vecteur_2.erase(vecteur_2.begin() + curseur);
    return vecteur_2;
}// enlever un �l�ment d'un vecteur

int main() {
    vector<vector<string>> ligne = {{"bonjour","hello","gutentag"},{"blabla","patate","fraise"}};
    vector<int> vect_int ={1,2,3,4,5,6,7,8,9};

    affiche_vect(vect_int);
    int it=2;

    vect_int=enleve_valeur(vect_int,8);

    affiche_vect(vect_int);






    return 0;
}
