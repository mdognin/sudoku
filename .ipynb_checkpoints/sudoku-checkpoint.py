import time

class Case: 
 
	def __init__(self,x,y,val,pos): 
		self.x = x 
		self.y = y 
		self.val = val 
		self.pos = pos 

	def affiche_grille(type):
		if type == "type1":
			global lig_temp 
			limite=[4,7,10] 
			for i in range (1,10): 
				if i in limite: 
					print("------ ------- ------") 
				lig_temp ="" 
				for j in range (1,10): 
					if j in limite: 
						exec("lig_temp+='|'+' '", globals()) 
					exec("lig_temp+=str(case{}{}.val)+' '".format(j,i), globals()) 
				print(lig_temp) 
			print()
		if type=="type2":
			for i in range(0,9): 
				print(vect_test.val[i*9:(i*9+9)]) 
	 
	def construct_case(): 
		global x, y, val, pos 
		x=1 #abscisse 
		y=1 #ordonnée 
		for it_ligne in grille: 
			if y == 10: 
				y = 1 
			for it_case in it_ligne: 
				val="n" 
				pos=() 
				if x==10: 
					x=1 
				 
				if it_case =="n": 
					pos=[1,2,3,4,5,6,7,8,9] 
				else: 
					val=it_case 
				exec("case{} = Case(x,y,val,pos)".format(str(x)+str(y)),  globals()) 
				x+=1 
			y+=1 

	def maj_pos_case(): 
		global val_impos 
		for i in range (1,10): 
			for j in range (1,10): 
				val_impos=[] 
				exec("val=case{}{}.val".format(j,i),  globals()) 
				if val == "n": 
					for it in li_val_col[j-1]: 
						val_impos.append(int(it)) 
					for it in li_val_lig[i-1]: 
						val_impos.append(int(it)) 
					if   j<4  and i<4:  num_car=1 
					elif j<7  and i<4:  num_car=2 
					elif j<10 and i<4:  num_car=3 
					elif j<4  and i<7:  num_car=4 
					elif j<7  and i<7:  num_car=5 
					elif j<10 and i<7:  num_car=6 
					elif j<4  and i<10: num_car=7 
					elif j<7  and i<10: num_car=8 
					elif j<10 and i<10: num_car=9 
					for it in li_val_car[num_car-1]: 
						val_impos.append(int(it)) 
					val_impos.sort() 
					#suppression des doublon de la liste valeur impossible 
					temp_li = []  
					for it in val_impos :  
						if it not in temp_li:  
							temp_li.append(it)  
					val_impos=temp_li.copy() 
					#val_impos.sort() 
					exec("pos=case{}{}.pos".format(j,i), globals())
  
					for it_impos in val_impos: 
						if it_impos in pos: 
							pos.remove(it_impos) 
					#print(j,i,"POS : ",pos,"IMPOS : ",val_impos) 

	def maj_val_case(): 
		flag_modif = False 
		for i in range (1,10): 
			for j in range (1,10): 
				exec("pos=case{}{}.pos".format(j,i), globals()) 
				exec("val=case{}{}.val".format(j,i), globals()) 
				if len(pos) == 1 and val =="n": 
					exec("case{}{}.val=str(pos[0])".format(j,i), globals()) 
					flag_modif = True 
		return flag_modif 
	pass 

class Col: 
	def __init__(self,num): 
		self.nom="col"+str(num) 
	 
	def construct_col(): 
		for i in range (1,10): 
			exec("col{}=Col({})".format(i,i), globals()) 
			for j in range (1,10): 
				exec("col{}.case{}=case{}{}".format(i,j,i,j), globals()) 
	 
	def maj_li_col(): 
		global val_col, li_val_col 
		for i in range (1,10): 
			val_col=[] 
			for j in range (1,10): 
				exec("val=col{}.case{}.val".format(i,j), globals()) 
				if val != "n": 
					val_col.append(val) 
			exec("val_col{}=val_col.copy()".format(i), globals()) 
		li_val_col=[val_col1,val_col2,val_col3,val_col4,val_col5,val_col6,val_col7,val_col8,val_col9] 
	 
	def verif_col(): 
		flag = True 
		for k in range(0,9): 
			for it_i in li_val_col.val[k]: 
				li_temp=li_val_col.val[k].copy() 
				li_temp.remove(it_i) 
				for it_j in li_temp: 
					if it_i==it_j: 
						flag = False
						break
		return flag 
		 
	pass 
  
class Car: 
	def __init__(self,case1,case2,case3,case4,case5,case6,case7,case8,case9): 
		self.case1=case1 
		self.case2=case2 
		self.case3=case3 
		self.case4=case4 
		self.case5=case5 
		self.case6=case6 
		self.case7=case7 
		self.case8=case8 
		self.case9=case9 

	pass 
		 
	def construct_car(): 
		for i in range(1,10): 
			if i==1:   j=1; k=1 
			elif i==2: j=1; k=4 
			elif i==3: j=1; k=7 
			elif i==4: j=4; k=1 
			elif i==5: j=4; k=4 
			elif i==6: j=4; k=7 
			elif i==7: j=7; k=1 
			elif i==8: j=7; k=4 
			elif i==9: j=7; k=7 
			exec("car{}=Car(lig{}.case{}, lig{}.case{} , lig{}.case{}, lig{}.case{} ,lig{}.case{}, lig{}.case{} ,lig{}.case{}, lig{}.case{},lig{}.case{})".format(i,j,k,j,k+1,j,k+2,j+1,k,j+1,k+1,j+1,k+2,j+2,k,j+2,k+1,j+2,k+2),globals()) 
	 
	def maj_li_car(): 
		global val_car, li_val_car 
		for i in range (1,10): 
			val_car=[] 
			for j in range (1,10): 
				exec("val=car{}.case{}.val".format(i,j), globals()) 
				if val != "n": 
					val_car.append(val) 
			exec("val_car{}=val_car.copy()".format(i), globals()) 
		li_val_car=[val_car1,val_car2,val_car3,val_car4,val_car5,val_car6,val_car7,val_car8,val_car9] 

	def verif_car():
		flag = True ; li_temp=[]
		for k in range(0,9):
			for it_i in li_val_car.val[k]: 
				li_temp=li_val_car.val[k].copy() 
				li_temp.remove(it_i) 
				for it_j in li_temp: 
					if it_i==it_j: 
						flag = False
						break
		return flag 
	 
	pass 
  
class Lig: 
	def __init__(self,num): 
		self.nom="lig"+str(num) 
		 
	def construct_lig(): 
		for i in range (1,10): 
			exec("lig{}=Lig({})".format(i,i), globals()) 
			for j in range (1,10): 
				exec("lig{}.case{}=case{}{}".format(i,j,j,i), globals()) 
	 

	def maj_li_lig():
		for i in range (1,10): 
			global val_lig, li_val_lig 
			val_lig=[] 
			for j in range (1,10): 
				exec("val=lig{}.case{}.val".format(i,j), globals()) 
				if val != "n": 
					val_lig.append(val) 
			exec("val_lig{}=val_lig.copy()".format(i), globals()) 
		li_val_lig=[val_lig1,val_lig2,val_lig3,val_lig4,val_lig5,val_lig6,val_lig7,val_lig8,val_lig9] 
	 
	def verif_lig(temp_lig): 
		flag = True 
		for it_i in temp_lig: 
			li_temp=temp_lig.copy() 
			li_temp.remove(it_i) 
			for it_j in li_temp: 
				if it_i==it_j: 
					flag = False
					break
		return flag 
	pass 

class Vecteur:
	def __init__(self): 
		self.val=[]
	
	def construct_vect_pos():
		for i in range (1,10): 
			for j in range (1,10): 
				exec("val_temp=case{}{}.val".format(j,i),globals()) 
				if val_temp != "n": 
					vect_pos.val.append([int(val_temp)]) 
					vect_pos_compt.val.append([1,0]) 
				else: 
					exec("pos_temp=case{}{}.pos".format(j,i),globals()) 
					vect_pos.val.append(pos_temp) 
					vect_pos_compt.val.append([len(pos_temp),0])

	def construct_lig_test(debut):
		lig_test=[]
		for i in range(debut,debut+9):
			#print(i,vect_pos_compt,vect_pos)
			compt_temp=vect_pos_compt.val[i][1] 
			lig_test.append(vect_pos.val[i][compt_temp])
		#print("fin")
		return lig_test

	def construct_vect_test():
		for i in range(80,-1,-1): 
			compt_temp=vect_pos_compt.val[i][1] 
			vect_test.val[i] = vect_pos.val[i][compt_temp]
		return vect_test.val

	def maj_vect_compt():
		vect_pos_compt.val[80][1]+=1
		for i in range (80,-1,-1):
			if vect_pos_compt.val[i][1] == vect_pos_compt.val[i][0]:
					vect_pos_compt.val[i][1]=0
					vect_pos_compt.val[i-1][1]+=1
			else:
				break

	def creation_lig(test):
		val_lig=[]
		val_lig.append(test)

	def creation_col():
		li_val_col.val=[]
		for i in range(0,9): 
			col_temp=[]
			for j in range(0,9): 
				col_temp.append(vect_test.val[j*9]) 
			li_val_col.val.append(col_temp)
	
	def creation_car():
		li_val_car.val=[]
		j=0
		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]

		for i in range(0,27):
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vect_test.val[i]) ; j+=1
			elif j<6 : car_temp2.append(vect_test.val[i]) ; j+=1
			elif j<9 : car_temp3.append(vect_test.val[i]) ; j+=1
		li_val_car.val.append(car_temp1)
		li_val_car.val.append(car_temp2)
		li_val_car.val.append(car_temp3)

		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]
		for i in range(27,54): 
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vect_test.val[i]) ; j+=1
			elif j<6 : car_temp2.append(vect_test.val[i]) ; j+=1
			elif j<9 : car_temp3.append(vect_test.val[i]) ; j+=1
		li_val_car.val.append(car_temp1)
		li_val_car.val.append(car_temp2)
		li_val_car.val.append(car_temp3)

		car_temp1=[] 
		car_temp2=[] 
		car_temp3=[]

		for i in range(54,81): 
			if j==9 :  j=0
			if j<3 :   car_temp1.append(vect_test.val[i]) ; j+=1
			elif j<6 : car_temp2.append(vect_test.val[i]) ; j+=1
			elif j<9 : car_temp3.append(vect_test.val[i]) ; j+=1
		li_val_car.val.append(car_temp1)
		li_val_car.val.append(car_temp2)
		li_val_car.val.append(car_temp3)
	pass

#def main():
''' 
grille=[["7","1","2", "3","8","4", "9","6","5"], 
		["8","5","6", "9","1","7", "4","3","2"], 
		["9","4","n", "6","5","2", "8","7","1"], 

		["2","9","8", "1","4","3", "6","5","7"], 
		["6","7","1", "8","9","5", "2","4","3"], 
		["5","3","4", "7","2","6", "1","8","9"], 

		["1","6","7", "2","3","8", "5","9","4"], 
		["4","8","9", "5","7","1", "3","2","6"], 
		["3","2","5", "4","6","9", "7","1","8"]]

grille=[["7","1","2", "3","8","4", "9","6","5"], 
		["n","5","6", "9","1","7", "n","3","n"], 
		["n","4","n", "n","5","n", "8","7","1"], 

		["n","9","n", "n","n","n", "6","n","n"], 
		["6","7","1", "n","9","5", "2","n","n"], 
		["n","n","n", "n","2","n", "1","n","n"], 

		["1","6","7", "n","3","n", "5","9","n"], 
		["4","8","n", "n","7","n", "3","n","n"], 
		["n","2","5", "4","6","n", "n","n","n"]]

''' 
grille=[["n","n","2", "n","8","n", "n","6","n"], 
		["n","5","6", "9","1","7", "n","3","n"], 
		["n","4","n", "n","5","n", "8","7","1"], 

		["n","9","n", "n","n","n", "6","n","n"], 
		["6","7","1", "n","9","5", "2","n","n"], 
		["n","n","n", "n","2","n", "1","n","n"], 

		["1","6","7", "n","3","n", "5","9","n"], 
		["4","8","n", "n","7","n", "3","n","n"], 
		["n","2","5", "4","6","n", "n","n","n"]] 
''' 
grille=[["n","n","n", "4","n","n", "n","n","n"], 
		["8","n","n", "n","n","3", "5","n","2"], 
		["n","3","n", "n","9","n", "n","6","n"], 

		["1","5","n", "n","8","n", "n","n","n"], 
		["4","n","n", "n","1","n", "3","n","n"], 
		["2","n","n", "n","3","n", "7","8","n"], 

		["n","n","n", "n","n","n", "n","3","n"], 
		["n","n","n", "n","7","n", "2","9","n"], 
		["n","n","n", "1","n","n", "n","n","6"]] 
''' 

start_time = time.time()
#Constructions des cases 
Case.construct_case() 
#Constructions des colonnes 
Col.construct_col() 
#Constructions des lignes 
Lig.construct_lig() 
#Constructions des carrées 
Car.construct_car() 

flag_modif =True 
m=0 

while flag_modif==True: 
	 
	#MAJ de la liste des chiffres de chaque colonne 
	Col.maj_li_col() 
	#MAJ de la liste des chiffres de chaque ligne 
	Lig.maj_li_lig() 
	#MAJ de la liste des chiffres de chaque carré 
	Car.maj_li_car() 
	 
	if m==2:#nbr_it_premier_cycle: 
		break
	#balayage de chaque case pour savoir s'il faut mettre à jour les possibilités 
	Case.maj_pos_case() 
	#balayage de toutes les cases pour mettre à jour les valeur si il ne reste qu'une seule possibilité
	flag_modif=Case.maj_val_case()

	m+=1

print("Nombre d'itérations etape 1 : ",m) 

Case.affiche_grille("type1")  

vect_pos=Vecteur()
vect_pos_compt=Vecteur()
Vecteur.construct_vect_pos()

vect_test=Vecteur()
vect_test.val=[0]*81

nbr_combinaison=1 

#Calcul nombre de possibilité
for i in range(0,81): 
	nbr_combinaison*=len(vect_pos.val[i]) 

end_time_etape1 = time.time()

print("Durée étape 1 : ",end_time_etape1-start_time)

print("Nombre de possibilité : ",nbr_combinaison)

flag_col_bon = False 
flag_lig_bon = False 
flag_car_bon = False 

#Boucle d'itération pour tester toute les solutions possibles
h=0
g=1000
avancement=[1000]
avancement_pourcent=0
for i in range(1,101):
	avancement.append(int(nbr_combinaison*(i/100)))

li_val_car=Vecteur()
li_val_col=Vecteur()
li_val_lig=Vecteur()

debut_boucle = time.time()

while flag_car_bon == False:
	h+=1

	flag_lig_bon == True

	for num_lig in range(0,9):
		#création de la ligne à tester
		
		lig_test=Vecteur.construct_lig_test(num_lig*9)

        if sum(lig_test)==45:
            flag_lig_bon=False
            break
		#Vérification de présence du chiffre dans la ligne
		flag_lig_bon = Lig.verif_lig(lig_test)
		#print(lig_test,"mm",num_lig,flag_lig_bon)

		if flag_lig_bon == False:
			break
		
	if flag_lig_bon == True:
		#creation du vecteur à tester
		vect_test.val=Vecteur.construct_vect_test()
		print("ligne OK")
		flag_lig_bon = False 
		#Création de la liste des colonnes de la grille qui doit être tester
		Vecteur.creation_col()
		#Vérification de présence du chiffre dans la colonne 
		flag_col_bon=Col.verif_col()
		
		if flag_col_bon == True:
			print("carré OK")
			flag_col_bon = False 
			#Création de la liste des carrés de la grille qui doit être tester
			Vecteur.creation_car()
			#Vérification de présence du chiffre dans le carré
			flag_car_bon=Car.verif_car()
			print("flag_car_bon",flag_car_bon)

	if h in avancement:

		if h==1000:
			print("h :",h)
			temp_time=1.01
			temp_time=(time.time()-debut_boucle)
			print("Durée moyenne 1000 itération : ",temp_time)
			print("Temps Maximum estimé : ",temp_time*nbr_combinaison)
		else :
			print("Avancement : ",avancement_pourcent," %")
			avancement_pourcent+=1
		avancement.remove(avancement[0])

	g+=1

	#décalage du compteur de 1
	Vecteur.maj_vect_compt()

print("Nombre d'itérations : ",h) 

Case.affiche_grille("type2") 
		
#nbr_it_premier_cycle = 3
#main()